section .text
 global parse_uint 
global parse_int
global string_length
global print_newline
global print_string
global read_word
global string_equals
global exit
global print_char
  global print_uint 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, rax,
	
	mov rax, 60
	syscall
	
    ret 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
 
 string_length:
    xor rax, rax
    
	.loop:
		cmp byte [rdi+rax], 0
		je .end
		inc rax
		jmp .loop
	.end:
		
		ret
		
; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    xor rax, rax

;положить в rdx колво символов
	call string_length
	mov rdx, rax
;положить в rsi указатель	
	mov rsi, rdi	
	mov rax, 1
	mov rdi, 1
	
	syscall
	
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
	
	push rdi ; сохраняем символ в стэк
    mov rsi, rsp ; откуда выводить (адрес вершины стэка)
	mov rax, 1
	mov rdx, 1
	mov rdi, 1
	syscall
	pop rdi
	
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
	
	
	mov rdi,  0xA
    call print_char
    
	ret


%define BUFFER_SIZE 32
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    push rbp ; сохраняем зачение rbp, положив его на стэк
    mov  rbp, rsp ; сохраняем в rbp адрес вершины стэка
    mov  rax, rdi ; сохраняем переданное число в "аккумулятор"
    mov  rdi, 10 ; заносим делитель (10) в rdi
    sub  rsp, BUFFER_SIZE ; выделяем буфер в стэке
    dec  rbp ; выделяем место под 0 на вершине стэка (нультермируем для вывода)
    mov  byte[rbp], 0 ; записываем 0 на вершину стэка
      .loop:
        dec  rbp ; сдвигаем метку для новой цифры
        xor  rdx, rdx ; обнуление rdx
        div  rdi ; целочисленное деление на 10 с записью в остатка в rdx
        add  rdx, '0' ; остаток + код первой цифры в ASCII
        mov  byte[rbp], dl ; сохраняем 1 байт (цифру) на стэк
        test rax, rax ; устанавливает флаги (ZF = 0, если rax = 0)
        jnz  .loop ; если rax !=0, продолжает цикл
    mov rdi, rbp ; помещаем аргумент для print_string (метку на стэке)
    call print_string ; выводим число в stdout
    add rsp, BUFFER_SIZE ; освобождаем буфер
	pop rbp ; восстанавливаем значение rbp
    ret



; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
xor rax, rax
  test rdi, rdi ; устанавливаем флаги
  
  jns  .unsigned ; переход, если беззнаковое
  push rdi ; сохраняем перед вызовом функции
 
  mov  rdi, '-'  ; помещаем аргумент
  call print_char ; выводим знак
  pop  rdi ; восстанавливаем значение rdi
  neg  rdi ; делаем число положительным (not + inc)
  
  .unsigned:
  call  print_uint ; обычный вывод беззнакового числа
  ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
	xor rcx, rcx
.loop:
	mov al, [rdi+rcx]
	cmp [rsi+rcx], al
	jne .return_not_equals
	test al, al
	jz .return_equals
	inc rcx
	jmp .loop
	
.return_equals:
	mov rax, 1
	ret
.return_not_equals:
	mov rax, 0
	ret










; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:

	push qword 0
    
	mov rdi , 0
	mov rax , 0
	mov rsi , rsp
	mov rdx , 1
	
	syscall
	
	pop rax
	
	test rax, rax
	js .eop
		
	ret
.eop:
	xor rax, rax
	ret













; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:

	push r12
	push r13
	
	push r14
	
	mov r12, rdi
	mov r13, rsi
	mov byte[r12], 0 
	
	.skip_char:
		call read_char
		test rax, rax
		jz .zero
		cmp rax, 32
		je .skip_char
		cmp rax, 10
		je .skip_char
		cmp rax, 9
		je .skip_char
	
	xor r14, r14
	.loop:
		cmp r14, r13
		jge .zero
		mov [r12+r14], al
		inc r14
		call read_char
		test rax, rax
		jz .word_end
		cmp rax, 32
		je .word_end
		cmp rax, 10
		je .word_end
		cmp rax, 9
		je .word_end
		jmp .loop
	
	.word_end:
		cmp r14, r13
		jge .zero
		mov byte [r12+r14], 0 ; Эта функция должна дописывать к слову нуль-терминатор
		mov rax, r12
		mov rdx, r14
		jmp .ret	
	
	
	.zero:
		xor rax, rax
		xor rdx, rdx
	.ret:
		pop r14
		pop r13
		pop r12
		ret
 
 
 
  
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rcx, rcx
	xor rsi, rsi
	
	mov r10, 10
.loop:
	mov sil, [rdi+rcx] ; 
	sub rsi, '0'
	js .exit
	cmp rsi, 9
	jg .exit
	mul r10
	add rax, rsi
	inc rcx
	jmp .loop
.exit:
	mov rdx, rcx
	ret
	
	
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
	xor rdx, rax
	
	cmp byte [rdi], '+'
	je .parse_pos
	cmp byte [rdi], '-'
	je .parse_neg
	call parse_uint
	ret
	
.parse_pos:
	inc rdi
	call parse_uint
	inc rdx
	ret
	
.parse_neg:
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret	
	
	 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
	
 ;   call string_length
 ;   mov  rcx, rax ; помещаем длину строки в rcx
 ;   inc  rcx ; увеличиваем, чтобы дойти до 0
 ;   xchg rsi, rdi ; меняем местами (в rdi теперь ссылка на место копирования)
 ;   mov  byte[rdi + rax], 0 ; нультермируем место, куда будем копировать строку
 ;   rep  movsb ; повторяем копирование(побайтово), используя rcx, как счетчик
 ; ;  1) movsb|movsw|movsd – копирует байт|слово|двойное_слово из памяти по адресу esi в память по адресу edi.
 ; ;  После выполнения команды регистры esi и edi увеличиваются на 1|2|4,
 ; ;  когда флаг DF=0, и уменьшаются, когда DF=1. Команда movs с префиксом rep выполняет копирование строки байтов|слов|двойных_слов длиной ecx
 ; ;
	push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jae .rez_high
    xor rax, rax
    mov r10b,0
.copy:
    mov r10b,byte[rdi+rax]
    mov [rsi+rax],r10b
    inc rax
    test r10b,r10b
    jnz .copy
    ret
.rez_high:
    mov rax,0
.exit:
    ret
