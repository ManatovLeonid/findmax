
all: main

main: max.o lib.o
	ld -o main max.o lib.o

max.o: max.asm 
	nasm -felf64 max.asm

lib.o: lib.asm 
	nasm -felf64 lib.asm

clean:
	rm -f lib.o max.o main
