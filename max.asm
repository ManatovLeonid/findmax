extern print_string
extern read_word
extern parse_int
extern exit
extern print_char
extern parse_uint
extern print_uint
extern print_newline
%define O_RDONLY 0
%define PROT_READ 0x1
%define MAP_PRIVATE 0x2

section .data
; This is the file name. You are free to change it.

fname: db 'input', 0
section .text
global _start

_start:

push r12 ; счетчик
push r13 ; максимум
push r14 ; укзаатель на строчку

; call open
mov rax, 2
mov rdi, fname
mov rsi, O_RDONLY ; Open file read only
mov rdx, 0 ; We are not creating a file
; so this argument has no meaning
syscall



mov r8, rax ; rax holds opened file descriptor
; it is the fourth argument of mmap
mov rax, 9 ; mmap number
mov rdi, 0 ; operating system will choose mapping destination
mov rsi, 4096 ; page size
mov rdx, PROT_READ ; new memory region will be marked read only
mov r10, MAP_PRIVATE ; pages will not be shared
mov r9, 0 ; offset inside test.txt
syscall ; now rax will point to mapped location
mov rdi, rax
mov r14, rax
call parse_uint
mov r12, rax
add r14, rdx
inc r14

; считать первый и занести в максимум
mov rdi, r14 
call parse_uint ;читаем
add r14, rdx ;сдвиг на следующее число
inc r14
mov r13,rax ; сохранить в максимум
dec r12	;уменьшить счетчик
jz .end

	.loop:
		
		mov rdi, r14 
		call parse_uint
		add r14, rdx ;сдвиг на следующее число
		inc r14 
		
		mov rdi, rax

		cmp rax, r13 ;проверка на максимум
		jl .continuee
		mov r13,rax
	.continuee:
		dec r12
		jz .end
		jmp .loop


.end:
mov rdi, r13
call print_uint
call print_newline
pop r14
pop r13
pop r12
call exit